﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace _4_DQNamHMThanNTLuat_LTNET
{
    internal class Function
    {
        private readonly string connectionString = "Data Source=NAMPOX\\SQLEXPRESS;Initial Catalog=db_Hotel;Integrated Security=True";

        protected SqlConnection GetConnection()
        {
            return new SqlConnection(connectionString);
        }

        public DataSet GetData(string query)
        {
            using (SqlConnection con = GetConnection())
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
        }

        public void SetData(string query, string message)
        {
            try
            {
                using (SqlConnection con = GetConnection())
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }

                MessageBox.Show(message, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public SqlDataReader GetForCombo(string query)
        {
            SqlConnection con = GetConnection();
            con.Open();
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader sdr = cmd.ExecuteReader(CommandBehavior.CloseConnection); // Đóng kết nối khi SqlDataReader đóng
            return sdr;
        }
    }
}
