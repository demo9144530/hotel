﻿using System;
using System.Data;
using System.Windows.Forms;

namespace _4_DQNamHMThanNTLuat_LTNET.main.Room
{
    public partial class StatusRoom : Form
    {
        Function fn = new Function();
        string query;

        public StatusRoom()
        {
            InitializeComponent();
        }

        private void StatusRoom_Load(object sender, EventArgs e)
        {
            query = "SELECT R.RoomID, R.RoomName, RT.TypeName, RT.Description, R.Status, RT.PricePerDay " +
                    "FROM Rooms R " +
                    "INNER JOIN RoomTypes RT ON R.RoomType = RT.RoomTypeID";

            DataSet ds = fn.GetData(query);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                itemRoom roomItem = new itemRoom();
                roomItem.SetRoomInfo(
                    row["RoomName"].ToString(),
                    GetStatusText(Convert.ToInt32(row["Status"])),
                    row["TypeName"].ToString(),
                    row["Description"].ToString(),
                    row["PricePerDay"].ToString()
                );
                flowLayoutPanel1.Controls.Add(roomItem);
            }
        }

        private string GetStatusText(int statusID)
        {
            string statusText = "Không xác định";

            if (statusID == 0)
            {
                statusText = "Phòng Trống";
            }
            else if (statusID == 1)
            {
                statusText = "Phòng Đã Đặt";
            }
            else if (statusID == 2)
            {
                statusText = "Phòng Đang Dọn Dẹp";
            }

            return statusText;
        }
    }
}
