﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4_DQNamHMThanNTLuat_LTNET.main.Room
{
    public partial class itemRoom : UserControl
    {
        public itemRoom()
        {
            InitializeComponent();
        }
        public void SetRoomInfo(string roomName, string status, string roomType, string description, string pricePerDay)
        {
            labelRoomName.Text = $"Phòng: {roomName}";
            labelStatus.Text = $"Trạng Thái: {status}";
            labelRoomType.Text = $"Loại Phòng: {roomType}";
            labelPricePerDay.Text = $"Giá Phòng: {int.Parse(pricePerDay):N0}"; // Định dạng giá tiền thành số nguyên (không có dấu thập phân)
        }


        private void itemRoom_Load(object sender, EventArgs e)
        {
            labelRoomName.Click -= labelRoomName_Click;
            labelRoomName.Click += labelRoomName_Click;
        }

        private void labelRoomName_Click(object sender, EventArgs e)
        {
            itemRoomDetail roomDetailForm = new itemRoomDetail();
            roomDetailForm.Show();
        }

        private void labelRoomType_Click(object sender, EventArgs e)
        {

        }

        private void labelStatus_Click(object sender, EventArgs e)
        {

        }

        private void labelPricePerDay_Click(object sender, EventArgs e)
        {

        }
    }
}
