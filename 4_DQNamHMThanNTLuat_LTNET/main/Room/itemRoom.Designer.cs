﻿namespace _4_DQNamHMThanNTLuat_LTNET.main.Room
{
    partial class itemRoom
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPricePerDay = new System.Windows.Forms.Label();
            this.labelRoomType = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.labelRoomName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelPricePerDay
            // 
            this.labelPricePerDay.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelPricePerDay.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelPricePerDay.Location = new System.Drawing.Point(0, 113);
            this.labelPricePerDay.Name = "labelPricePerDay";
            this.labelPricePerDay.Size = new System.Drawing.Size(285, 35);
            this.labelPricePerDay.TabIndex = 8;
            this.labelPricePerDay.Text = "loại phòng";
            this.labelPricePerDay.Click += new System.EventHandler(this.labelPricePerDay_Click);
            // 
            // labelRoomType
            // 
            this.labelRoomType.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRoomType.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelRoomType.Location = new System.Drawing.Point(0, 74);
            this.labelRoomType.Name = "labelRoomType";
            this.labelRoomType.Size = new System.Drawing.Size(285, 39);
            this.labelRoomType.TabIndex = 7;
            this.labelRoomType.Text = "giá phòng";
            this.labelRoomType.Click += new System.EventHandler(this.labelRoomType_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelStatus.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelStatus.Location = new System.Drawing.Point(0, 33);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(285, 41);
            this.labelStatus.TabIndex = 6;
            this.labelStatus.Text = "Trạng Thái";
            this.labelStatus.Click += new System.EventHandler(this.labelStatus_Click);
            // 
            // labelRoomName
            // 
            this.labelRoomName.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.labelRoomName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelRoomName.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.labelRoomName.ForeColor = System.Drawing.SystemColors.Desktop;
            this.labelRoomName.Location = new System.Drawing.Point(0, 0);
            this.labelRoomName.Name = "labelRoomName";
            this.labelRoomName.Size = new System.Drawing.Size(285, 33);
            this.labelRoomName.TabIndex = 5;
            this.labelRoomName.Text = "Tên phòng";
            this.labelRoomName.Click += new System.EventHandler(this.labelRoomName_Click);
            // 
            // itemRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.labelPricePerDay);
            this.Controls.Add(this.labelRoomType);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.labelRoomName);
            this.Name = "itemRoom";
            this.Size = new System.Drawing.Size(285, 154);
            this.Load += new System.EventHandler(this.itemRoom_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelPricePerDay;
        private System.Windows.Forms.Label labelRoomType;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelRoomName;
    }
}
