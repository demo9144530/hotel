﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4_DQNamHMThanNTLuat_LTNET
{
    public partial class RoomTypes : Form
    {
        Function fn = new Function();
        string query;

        public RoomTypes()
        {
            InitializeComponent();
        }

        private void RoomTypes_Load(object sender, EventArgs e)
        {
            RefreshDataGridView();
            int roomIDColumnIndex = dataGridView1.Columns["RoomTypeID"].Index;

            dataGridView1.Columns[roomIDColumnIndex].Visible = false;
        }

        private void RefreshDataGridView()
        {
            query = "SELECT RoomTypeID, TypeName, PricePerDay, Description FROM RoomTypes";
            // Sử dụng hàm GetData để lấy dữ liệu từ CSDL và gán nó cho dataGridView1
            DataSet ds = fn.GetData(query);
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void btnAddType_Click(object sender, EventArgs e)
        {
            if (txtTypeName.Text != "" && txtPrice.Text != "")
            {
                string typeName = txtTypeName.Text;
                decimal price = Convert.ToDecimal(txtPrice.Text);
                string description = txtDescription.Text;
                query = $"INSERT INTO RoomTypes (TypeName, PricePerDay, Description) " +
                    $"VALUES (N'{typeName}', {price}, N'{description}')";
                fn.SetData(query, "Loại phòng đã được thêm thành công.");
                txtTypeName.Clear();
                txtPrice.Clear();
                txtDescription.Clear();

                // Sau khi thêm thành công, gọi phương thức RefreshDataGridView để cập nhật dataGridView1
                RefreshDataGridView();

            }
            else
            {
                MessageBox.Show("Vui lòng điền đầy đủ thông tin loại phòng.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDeleteType_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int roomTypeID = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["RoomTypeID"].Value);
                query = $"DELETE FROM RoomTypes WHERE RoomTypeID = {roomTypeID}";
                fn.SetData(query, "Loại phòng đã được xóa thành công.");
                    RefreshDataGridView();
            }
            else
            {
                MessageBox.Show("Vui lòng chọn một loại phòng để xóa.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int roomTypeID = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["RoomTypeID"].Value);

                if (txtTypeName.Text != "" && txtPrice.Text != "")
                {
                    string typeName = txtTypeName.Text;
                    decimal price = Convert.ToDecimal(txtPrice.Text);
                    string description = txtDescription.Text;

                    query = $"UPDATE RoomTypes " +
                            $"SET TypeName = N'{typeName}', PricePerDay = {price}, Description = N'{description}' " +
                            $"WHERE RoomTypeID = {roomTypeID}";
                    fn.SetData(query, "Loại phòng đã được cập nhật thành công.");
                    RefreshDataGridView();
                }
                else
                {
                    MessageBox.Show("Vui lòng điền đầy đủ thông tin loại phòng.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn một loại phòng để sửa.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dataGridView1.SelectedRows[0];
                txtTypeName.Text = selectedRow.Cells["TypeName"].Value.ToString();
                txtPrice.Text = selectedRow.Cells["PricePerDay"].Value.ToString();
                txtDescription.Text = selectedRow.Cells["Description"].Value.ToString();
            }
        }
    }
}
