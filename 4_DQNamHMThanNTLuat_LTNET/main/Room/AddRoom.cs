﻿using System;
using System.Data;
using System.Windows.Forms;

namespace _4_DQNamHMThanNTLuat_LTNET
{
    public partial class AddRoom : Form
    {
        Function fn = new Function();
        string query;

        public AddRoom()
        {
            InitializeComponent();
        }

        private void AddRoom_Load(object sender, EventArgs e)
        {
            query = "SELECT R.RoomID, R.RoomName, RT.TypeName, RT.Description, R.Status, RT.PricePerDay " +
                    "FROM Rooms R " +
                    "INNER JOIN RoomTypes RT ON R.RoomType = RT.RoomTypeID";

            DataSet ds = fn.GetData(query);
            dataGridView1.DataSource = ds.Tables[0];

            int roomIDColumnIndex = dataGridView1.Columns["RoomID"].Index;
            dataGridView1.Columns[roomIDColumnIndex].Visible = false;

           
            query = "SELECT RoomTypeID, TypeName FROM RoomTypes";
            DataSet roomTypes = fn.GetData(query);

            
            ComboBoxRoomCategory.DataSource = roomTypes.Tables[0];
            ComboBoxRoomCategory.DisplayMember = "TypeName";
            ComboBoxRoomCategory.ValueMember = "RoomTypeID";
            ComboBoxRoomCategory.SelectedIndex = -1;
        }


        private void btnAddRoom_Click(object sender, EventArgs e)
        {
            if (txtRoomNumber.Text != "" && ComboBoxRoomCategory.SelectedIndex != -1)
            {
                string roomNumber = txtRoomNumber.Text;
                int roomCategoryID = (int)ComboBoxRoomCategory.SelectedValue; // Lấy RoomTypeID từ ComboBox

                query = $"INSERT INTO Rooms (RoomName, RoomType, Status) " +
                        $"VALUES ('{roomNumber}', {roomCategoryID}, 0)";

                fn.SetData(query, "Phòng đã được thêm thành công.");
                txtRoomNumber.Clear();
                ComboBoxRoomCategory.SelectedIndex = -1; // Đặt lại ComboBox
            }
            else
            {
                MessageBox.Show("Vui lòng điền đầy đủ thông tin phòng.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private string GetStatusText(int statusID)
        {
            // Hãy thêm mã để chuyển đổi ID trạng thái thành văn bản tương ứng
            // Ví dụ:
            if (statusID == 0)
            {
                return "Phòng Trống";
            }
            else if (statusID == 1)
            {
                return "Phòng Đã Đặt";
            }
            else if (statusID == 2)
            {
                return "Phòng Đang Dọn Dẹp";
            }
            // Thêm các trạng thái khác nếu cần

            return "Không xác định";
        }
        

        private void dataGridView1_CellFormatting_1(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["Status"].Index && e.Value != null)
            {
                // Lấy giá trị trạng thái từ cột Status
                int statusID = Convert.ToInt32(e.Value);

                // Chuyển đổi ID trạng thái thành văn bản
                string statusText = GetStatusText(statusID);

                e.Value = statusText;
            }
        }
        private void btnDeleteRoom_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                int roomID = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells["RoomID"].Value);

                
                query = $"DELETE FROM Rooms WHERE RoomID = {roomID}";
                fn.SetData(query, "Phòng đã được xóa thành công.");

                // Gọi phương thức RefreshDataGridView bằng cách sử dụng phương thức Invoke
                if (dataGridView1.InvokeRequired)
                {
                    dataGridView1.Invoke(new Action(() => RefreshDataGridView()));
                }
                else
                {
                    RefreshDataGridView();
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn một phòng để xóa.", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void RefreshDataGridView()
        {
            DataSet ds = fn.GetData(query);
        }

        private void ComboBoxRoomCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
