﻿using _4_DQNamHMThanNTLuat_LTNET.main.Room;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4_DQNamHMThanNTLuat_LTNET.main
{
    public partial class home : Form
    {
        bool menuExpand = false;
        bool sidebarExpand = true;



        public home()
        {
            InitializeComponent();
        }

        private void menuTransition_Tick(object sender, EventArgs e)
        {
            if(menuExpand == false)
            {
                menuContainer.Height += 10;
                    if(menuContainer.Height >= 200)
                {
                    menuTransition.Stop();
                    menuExpand = true;
                }
            }
            else
            {
                menuContainer.Height -= 10;
                    if (menuContainer.Height <= 85)

                {
                    menuTransition.Stop();
                    menuExpand = false;
                }
            }
        }

        private void sidebarTransition_Tick(object sender, EventArgs e)
        {
            if (sidebarExpand)
            {
                sidebar.Width -= 10;
                if (sidebar.Width <= 60)
                {
                    sidebarExpand = false;
                    sidebarTransition.Stop();
                }
            }
            else
            {
                sidebar.Width += 10;
                if (sidebar.Width >= 295)
                {   
                    sidebarExpand = true;
                    sidebarTransition.Stop();
                }
            }
        }

        private void btnHam_Click(object sender, EventArgs e)
        {
            sidebarTransition.Start();
        }

        private void Menu_Click(object sender, EventArgs e)
        {
            labelRoom.Text = "Quản Lý Phòng";
            guna2PictureBoxRoom.Image = Properties.Resources.room_key;
            container(new StatusRoom());
            menuTransition.Start();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnDashboard_Click(object sender, EventArgs e)
        {
            labelRoom.Text = "Bảng Điều Khiển";
            guna2PictureBoxRoom.Image = Properties.Resources.room_key;
            container(new Home_index());
        }
        private void container(Object _form)
        {
            if(guna2Panel1.Controls.Count > 0) guna2Panel1.Controls.Clear();
            Form fm = _form as Form;
            fm.TopLevel = false;
            fm.FormBorderStyle = FormBorderStyle.None;
            fm.Dock = DockStyle.Fill;
            guna2Panel1.Controls.Add(fm);
            guna2Panel1.Tag = fm;
            fm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //labelRoom.Text = "Thêm phòng";
            //guna2PictureBoxRoom.Image = Properties.Resources.room_key;
            //container(new AddRoom());
        }


        // trạng thái phòng
        private void button7_Click(object sender, EventArgs e)
        {
            labelRoom.Text = "Danh Sách Phòng";
            guna2PictureBoxRoom.Image = Properties.Resources.booking;
            container(new AddRoom());
            menuHomeStatus.Start();
        }

        private void menuHomeStatus_Tick(object sender, EventArgs e)
        {
            //if (menuExpand == false)
            //{
            //    MenuStatus.Height += 10;
            //    // if(menuContainer.Height >= 253)
            //    if (MenuStatus.Height >= 185)
            //    {
                    
            //        menuExpand = true;
            //        menuHomeStatus.Stop();
            //    }
            //}
            //else
            //{
            //    MenuStatus.Height -= 10;
            //    //  if (menuContainer.Height <= 85)
            //    if (MenuStatus.Height <= 56)

            //    {
                    
            //        menuExpand = false;
            //        menuHomeStatus.Stop();
            //    }
            //}
        }

        private void btnRoomPytes_Click(object sender, EventArgs e)
        {
            labelRoom.Text = "Loại Phòng";
            guna2PictureBoxRoom.Image = Properties.Resources.booking;
            container(new RoomTypes());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            labelRoom.Text = "Danh Sách Khách Hàng";
            guna2PictureBoxRoom.Image = Properties.Resources.booking;
           //container(new RoomTypes());
            menuUserTransition.Start();
        }

        private void menuUserTransition_Tick(object sender, EventArgs e)
        {
            if (menuExpand == false)
            {
                MenuContainerUser.Height += 10;
                if (MenuContainerUser.Height >= 200)
                {
                    menuUserTransition.Stop();
                    menuExpand = true;
                }
            }
            else
            {
                MenuContainerUser.Height -= 10;
                if (MenuContainerUser.Height <= 75)

                {
                    menuUserTransition.Stop();
                    menuExpand = false;
                }
            }
        }
    }
}
