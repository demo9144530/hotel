﻿using _4_DQNamHMThanNTLuat_LTNET.main;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4_DQNamHMThanNTLuat_LTNET
{
    public partial class login : Form
    {
        int pw;
        bool hided;
        public login()
        {
            InitializeComponent();
            pw = panel5.Width;
            hided = false;
        }
        void IncreaseOpaccity(object sender, EventArgs e)
        {
            if(this.Opacity <=1)
            {
                this.Opacity += 0.01;
            }
            if (this.Opacity == 1)
            {
                time.Stop();
            }
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtUserName.Text == "")
                {
                    txtUserName.Text = "Enter Username";
                    return;
                }
                ChangeTextColor(txtUserName);
                panel5.Visible = false;
            }
            catch
            {

            }
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtPassWord.Text == "")
                {
                    return;
                }
                ChangeTextColor(txtPassWord);
                txtPassWord.PasswordChar = '*';
                panel7.Visible = false;
            }
            catch
            {

            }
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMgs, int wParam, int lParam);
        private void button1_Click(object sender, EventArgs e)
        {
            if(txtUserName.Text== "Enter Username")
            {
                panel5.Visible = true;
                txtUserName.Focus();
                return;
            }
            if (txtPassWord.Text == "Password")
            {
                panel7.Visible = true;
                txtPassWord.Focus();
                return;
            }
            if (txtUserName.Text == "admin" && txtPassWord.Text == "admin")
            {
                home home = new home();
                this.Hide();
                home.Show();
            }

        }
        
        private void textBox1_Click(object sender, EventArgs e)
        {
            txtUserName.SelectAll();
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            txtPassWord.SelectAll();
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            btnLogin.ForeColor = Color.Black;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            Color newColor = Color.FromArgb(88, 73, 16);
            btnLogin.ForeColor = newColor;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_MouseDown(object sender, MouseEventArgs e)
        {
            button3.ForeColor = Color.Black;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            Color newColor = Color.FromArgb(88, 73, 16);
            button3.ForeColor = newColor;
        }

        System.Windows.Forms.Timer time = new System.Windows.Forms.Timer();
        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox3.Text == "Nhập tên tài khoản")
            {
                pnlUsername.Visible = true;
                textBox3.Focus();
                textBox3.SelectAll();
                return;
            }
                if (textBox4.Text == "Nhập số điện thoại")
                {
                    pnlContact.Visible = true;
                textBox4.Focus(); textBox4.SelectAll();
                    return;
                }
                if (textBox5.Text == "Nhập mật khẩu")
                {
                    pnlPassword.Visible = true;
                textBox5.Focus(); textBox5.SelectAll();
                    return;
                }
                if (textBox6.Text == "Nhập lại mật khẩu")
                {
                    pnlCPassword.Visible = true;
                textBox6.Focus(); textBox6.SelectAll();
                    return;
                }
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pnlCPassword.Visible = pnlContact.Visible = pnlPassword.Visible = pnlUsername.Visible = false;
        }
        private void login_Load(object sender, EventArgs e)
        {
            this.Opacity = .01;
            time.Interval = 10;
            time.Tick += IncreaseOpaccity;
            time.Start();
        }

        private void panel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ChangeTextColor(TextBox textBox)
        {
            Color newColor = Color.FromArgb(88, 73, 16); // Màu RGB tương ứng với #584910
            textBox.ForeColor = newColor;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            ChangeTextColor(textBox3);
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            ChangeTextColor(textBox4);
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            ChangeTextColor(textBox5);
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            ChangeTextColor(textBox6);
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pnlLogin.Visible = true;
            pnlLogin.Dock = DockStyle.Fill;
            pnlSignup.Visible = false;
            pnlLogo.Dock = DockStyle.Left;

        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            pnlLogin.Visible = false;
            pnlSignup.Visible = true;
            pnlSignup.Dock = DockStyle.Fill;
            pnlLogo.Dock = DockStyle.Right;
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void login_Load_1(object sender, EventArgs e)
        {
            this.Opacity = .01;
            time.Interval = 10;
            time.Tick += IncreaseOpaccity;
            time.Start();
        }

        private void login_DragEnter(object sender, DragEventArgs e)
        {
            this.Opacity = .5;
        }

        private void login_ResizeBegin(object sender, EventArgs e)
        {
            this.Opacity = .5;
        }

        private void login_ResizeEnd(object sender, EventArgs e)
        {
            this.Opacity = 1;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
